package com.appgradingtechnology.labyalo.android.fragment.register;

import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.appgradingtechnology.labyalo.R;
import com.appgradingtechnology.labyalo.android.activity.RegisterActivity;
import com.appgradingtechnology.labyalo.android.activity.RegisterActivity;
import com.appgradingtechnology.labyalo.config.Keys;
import com.appgradingtechnology.labyalo.server.request.Auth;
import com.appgradingtechnology.labyalo.vendor.android.base.BaseFragment;
import com.appgradingtechnology.labyalo.vendor.server.request.APIRequest;
import com.appgradingtechnology.labyalo.vendor.server.transformer.BaseTransformer;
import com.appgradingtechnology.labyalo.vendor.server.util.ErrorResponseManger;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import butterknife.BindView;
import butterknife.OnClick;

/**
 * Created by Labyalo on 8/12/2017.
 */

public class SignUpFragment extends BaseFragment {
    public static final String TAG = SignUpFragment.class.getName().toString();

    private RegisterActivity registerActivity;


    public static SignUpFragment newInstance() {
        SignUpFragment fragment = new SignUpFragment();
        return fragment;
    }

    @BindView(R.id.signUpBTN)                       TextView signUpBTN;

    @BindView(R.id.fnameET)                         EditText fnameET;
    @BindView(R.id.lnameET)                         EditText lnameET;
    @BindView(R.id.contactET)                       EditText contact_numberET;
    @BindView(R.id.addressET)                       EditText street_addressET;

    @Override
    public int onLayoutSet() {
        return R.layout.fragment_signup;
    }

    @Override
    public void onViewReady() {
        registerActivity = (RegisterActivity) getContext();
    }

    @OnClick(R.id.signUpBTN)
    void onClick(View view){
        switch (view.getId()){
            case R.id.signUpBTN:
                APIRequest  apiRequest = Auth.getDefault().register(registerActivity)
                        .addParameter(Keys.FIRST_NAME, fnameET.getText().toString().trim())
                        .addParameter(Keys.LAST_NAME, lnameET.getText().toString().trim())
                        .addParameter(Keys.CONTACT_NUMBER, contact_numberET.getText().toString().trim())
                        .addParameter(Keys.STREET_ADDRESS, street_addressET.getText().toString().trim());
                        apiRequest.execute();

                        break;
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        registerActivity.setTitle("Sign Up");
    }

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        EventBus.getDefault().unregister(this);
        super.onStop();
    }

    @Subscribe
    public void onResponse(Auth.RegisterResponse registerResponse){
        BaseTransformer transformer = registerResponse.getData(BaseTransformer.class);
        if(transformer.status){
            Log.d("registerResponse", transformer.msg);
        }
        else{
            if(!ErrorResponseManger.first(transformer.error.fname).equals("")){
                fnameET.setError(ErrorResponseManger.first(transformer.error.fname));
            }
            if(!ErrorResponseManger.first(transformer.error.lname).equals("")){
                lnameET.setError(ErrorResponseManger.first(transformer.error.lname));
            }
            if(!ErrorResponseManger.first(transformer.error.contactNumber).equals("")){
                contact_numberET.setError(ErrorResponseManger.first(transformer.error.contactNumber));
            }
            if(!ErrorResponseManger.first(transformer.error.streetAddress).equals("")){
                street_addressET.setError(ErrorResponseManger.first(transformer.error.streetAddress));
            }
        }
    }

}
