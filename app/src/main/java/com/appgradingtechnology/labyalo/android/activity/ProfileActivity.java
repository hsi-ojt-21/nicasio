package com.appgradingtechnology.labyalo.android.activity;


import com.appgradingtechnology.labyalo.R;
import com.appgradingtechnology.labyalo.android.fragment.profile.UpdateProfileFragment;
import com.appgradingtechnology.labyalo.android.route.RouteActivity;

public class ProfileActivity extends RouteActivity {
    public static final String TAG = ProfileActivity.class.getName().toString();


    @Override
    public int onLayoutSet() {
        return R.layout.activity_profile;
    }

    @Override
    public void onViewReady() {

    }

    @Override
    public void initialFragment(String activityName, String fragmentName) {


        switch (fragmentName){
            case "update_profile":
                openUpdateProfileFragment();
                break;

        }
    }

    public void openUpdateProfileFragment(){ switchFragment(UpdateProfileFragment.newInstance()); }


}
