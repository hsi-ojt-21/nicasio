package com.appgradingtechnology.labyalo.android.fragment.landing;

import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.appgradingtechnology.labyalo.R;
import com.appgradingtechnology.labyalo.android.activity.LandingActivity;
import com.appgradingtechnology.labyalo.android.dialog.DefaultDialog;
import com.appgradingtechnology.labyalo.server.request.Auth;
import com.appgradingtechnology.labyalo.vendor.android.base.BaseFragment;
import com.appgradingtechnology.labyalo.vendor.server.transformer.BaseTransformer;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import butterknife.BindView;

/**
 * Created by Labyalo on 8/12/2017.
 */

public class LoginFragment extends BaseFragment implements View.OnClickListener {
    public static final String TAG = LoginFragment.class.getName().toString();

    private LandingActivity landingActivity;

    @BindView(R.id.contactET)           EditText contactET;
    @BindView(R.id.pinET)               EditText pinET;
    @BindView(R.id.registerBTN)         TextView registerBTN;

    //@BindView(R.id.emailET)           EditText emailET;
    //@BindView(R.id.passET)            EditText passET;
    @BindView(R.id.loginBTN)            TextView loginBTN;
    //@BindView(R.id.signUpBTN)         TextView signUpBTN;

    public static LoginFragment newInstance() {
        LoginFragment fragment = new LoginFragment();
        return fragment;
    }

    @Override
    public int onLayoutSet() {
        return R.layout.fragment_login;
    }

    @Override
    public void onViewReady() {
        landingActivity = (LandingActivity) getContext();
        loginBTN.setOnClickListener(this);
        registerBTN.setOnClickListener(this);
    }

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        EventBus.getDefault().unregister(this);
        super.onStop();
    }

    @Subscribe
    public void onResponse(Auth.LoginResponse loginResponse){
        Log.e("Message", loginResponse.getData(BaseTransformer.class).msg);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.loginBTN:
                Auth.getDefault().login(landingActivity,
                        contactET.getText().toString().trim(),
                        pinET.getText().toString().trim());


                break;
            case R.id.registerBTN:
                landingActivity.startRegisterActivity("signup");
                break;


        }

    }
}
