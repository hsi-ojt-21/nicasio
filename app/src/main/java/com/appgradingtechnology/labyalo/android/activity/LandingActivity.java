package com.appgradingtechnology.labyalo.android.activity;

import com.appgradingtechnology.labyalo.R;
import com.appgradingtechnology.labyalo.android.fragment.DefaultFragment;
import com.appgradingtechnology.labyalo.android.fragment.landing.LoginFragment;
import com.appgradingtechnology.labyalo.android.fragment.landing.SplashFragment;
import com.appgradingtechnology.labyalo.android.fragment.register.SignUpFragment;
import com.appgradingtechnology.labyalo.android.route.RouteActivity;

/**
 * Created by Labyalo on 8/12/2017.
 */

public class LandingActivity extends RouteActivity {
    public static final String TAG = LandingActivity.class.getName().toString();

    @Override
    public int onLayoutSet() {
        return R.layout.activity_landing;
    }

    @Override
    public void onViewReady() {

    }

    @Override
    public void initialFragment(String activityName, String fragmentName) {
        switch (fragmentName){
            case "login":
                openLoginFragment();
                break;
            default:
                openSplashFragment();
                break;
        }
    }

    public void openLoginFragment(){
        switchFragment(LoginFragment.newInstance());
    }
    public void openSignUpFragment(){ switchFragment(SignUpFragment.newInstance()); }
    public void openSplashFragment(){ switchFragment(SplashFragment.newInstance()); }
}
