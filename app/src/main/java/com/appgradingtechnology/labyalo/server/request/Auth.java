package com.appgradingtechnology.labyalo.server.request;

import android.content.Context;

import com.appgradingtechnology.labyalo.config.Keys;
import com.appgradingtechnology.labyalo.config.Url;
import com.appgradingtechnology.labyalo.data.model.api.SampleModel;
import com.appgradingtechnology.labyalo.data.model.api.UserModel;
import com.appgradingtechnology.labyalo.vendor.server.request.APIRequest;
import com.appgradingtechnology.labyalo.vendor.server.request.APIResponse;
import com.appgradingtechnology.labyalo.vendor.server.transformer.BaseTransformer;
import com.appgradingtechnology.labyalo.vendor.server.transformer.SingleTransformer;

import org.greenrobot.eventbus.EventBus;

import java.util.List;

import okhttp3.MultipartBody;
import retrofit2.Call;
import retrofit2.http.Header;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.Path;

/**
 * Created by Labyalo on 8/3/2017.
 */

public class Auth {

    public static Auth getDefault(){
        return new Auth();
    }

    public void login(Context context, String contactNumber, String password) {
        APIRequest apiRequest = new APIRequest<SingleTransformer<UserModel>>(context) {
            @Override
            public Call<SingleTransformer<UserModel>> onCreateCall() {
                return getRetrofit().create(RequestService.class).requestSingleTransformer(Url.getLogin(),
                        getAuthorization(), getMultipartBody());
            }

            @Override
            public void onResponse() {
                EventBus.getDefault().post(new LoginResponse(this));
            }
        };

        apiRequest
                .addParameter(Keys.CONTACT_NUMBER, contactNumber)
                .addParameter(Keys.PASSWORD, password)
                .addParameter(Keys.INCLUDE, "avatar")
                .showDefaultProgressDialog("Logging in...")
                .execute();

    }

    public APIRequest register(Context context) {
        APIRequest apiRequest = new APIRequest<BaseTransformer>(context) {
            @Override
            public Call<BaseTransformer> onCreateCall(){
                return getRetrofit().create(RequestService.class).requestBaseTransformer(Url.getRegistration(),
                        getAuthorization(), getMultipartBody());
            }
            @Override
            public void onResponse() {EventBus.getDefault().post(new RegisterResponse(this)); }
        };

        apiRequest
                .showDefaultProgressDialog("Registering...");
        return apiRequest;
    }


    public static void logout(int id) {

    }

    public void signup(SampleModel userModel) {

    }

    public static void facebook(SampleModel userModel) {

    }


    public interface RequestService {
        @Multipart
        @POST("{p}")
        Call<BaseTransformer> requestLogin(@Part List<MultipartBody.Part> parts);

        @POST("url")
        Call<BaseTransformer> requestLogout(@Header("Authorization") String authorization);

        @Multipart
        @POST("{p}")
        Call<SingleTransformer<UserModel>> requestSingleTransformer(@Path(value = "p", encoded = true) String p,
                                                                    @Header("Authorization") String authorization,
                                                                    @Part List<MultipartBody.Part> part);

        @Multipart
        @POST("{p}")
        Call<BaseTransformer> requestBaseTransformer(@Path(value = "p", encoded = true) String p,
                                                     @Header("Authorization") String authorization,
                                                     @Part List<MultipartBody.Part> part);
    }


    public class LoginResponse extends APIResponse<SingleTransformer<UserModel>> {
        public LoginResponse(APIRequest apiRequest) {
            super(apiRequest);
        }
    }

    public class RegisterResponse extends APIResponse<SingleTransformer<UserModel>> {
        public RegisterResponse(APIRequest apiRequest) {
            super(apiRequest);
        }
    }
}
